import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ApolloClient, HttpLink, InMemoryCache } from "apollo-boost"
import { ApolloProvider } from "react-apollo"
import Banner from "./src/Banner/bannerSlider"
import Loading from "./src/Component/loading"

const client = new ApolloClient({
  link: new HttpLink({ uri: 'https://www.supermarketreksadana.com/node' }),
  cache: new InMemoryCache()
})

export default class App extends React.Component {
  async componentWillMount() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });
  }

  render() {
    return (
      <ApolloProvider client={client}>
        <View style={styles.container}>
          <Banner />
        </View>
      </ApolloProvider>
    );
  }
} 

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
