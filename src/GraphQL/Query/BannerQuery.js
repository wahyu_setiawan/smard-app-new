import gql from "graphql-tag";

export const GetBanner = gql`
query tb_banner($id: String ){
    tb_banner(id: $id ){
        id
        image
        title
    }
}
`