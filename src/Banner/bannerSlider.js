import React from "react"
import { Card, } from "native-base";
import { } from "native-base";
import { Text, View, ScrollView, Image, StyleSheet } from "react-native";
import { Query } from "react-apollo";
import { GetBanner } from "../GraphQL/Query/BannerQuery";
import Loading from "../Component/loading"
import { GLOBAL } from "../../global"


const Banner = () => {
  return (
    <Query query={GetBanner}>
      {
        ({ loading, error, data }) => {

          // Loading for request data from graphQL
          if (loading) {
            return <Loading />
          }

          // error will be show in console log 
          if (error) {
            console.log(error)
            return <View><Text>Error</Text></View>
          }

          return (

            <ScrollView style={style.viewContainer} horizontal={true} showsHorizontalScrollIndicator={false}>
              {data.tb_banner.map((sliders) => (
                <View key={sliders.id} style={{
                  height: 130, width: 260, marginRight: 10,
                  marginLeft: 10, borderWidth: 0.5, borderColor: '#dddddd', borderRadius: 10
                }}>
                  <View style={{ flex: 2 }}>
                    <Image source={{ uri: GLOBAL.configURL + sliders.image }}
                      style={{ flex: 1, width: null, height: null, resizeMode: 'cover', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}
                    />
                  </View>
                  <View style={{ flex: 1, paddingLeft: 10, paddingTop: 10 }}>
                    <Text>{sliders.title}</Text>
                  </View>
                </View>
              ))}
            </ScrollView>
          )
        }
      }
    </Query>
  )
};

const style = StyleSheet.create({
  viewContainer: {
    paddingTop: 5,
    paddingLeft: 10,
    paddingRight: 60,
    paddingBottom: 5,
    marginRight: 12
  }
})

export default Banner;
